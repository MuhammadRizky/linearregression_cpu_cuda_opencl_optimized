#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>

//CUDA RUNTIME
#include <cuda_runtime.h>
#include <helper/helper_functions.h>
#include <helper/helper_cuda.h>
// #include <cooperative_groups.h>


// #define WARPSIZE 32

int readDataFile(char* filen, int num, float* x, float* y);
int computeCost(float &out, float* x, float* y, float* theta, int m);
int gradientDescent(float* J_hist, float* x, float* xWOOne, float* y, float* theta, int m, float alpha, int iter, float* tHist);
__global__ void computeCostGPU_CUDA(float &result, float* jCost, float* X, float* Y, float* theta, int m);
__global__ void computeCostGPU_CUDA_SMem(float &result, float* jCost, float* X, float* Y, float* theta, int m);
__global__ void computeCostGPU_CUDA_SMem2(float &result, float* jCost, float* X, float* Y, float* theta, int m);
__global__ void computeCostGPU_CUDA_SMem3(float &result, float* jCost, float* X, float* Y, float* theta, int m, int numBlockShared);
__global__ void computeCostGPU_CUDA_SMem1b1t(float &result, float* X, float* Y, float* theta, int m, int iter);
__global__ void computeCostGPU_CUDA_SMem1b1t_static(float &result, float* X, float* Y, float* theta, int m, int iter);
__global__ void computeCostGPU_CUDA(float &result, float* X, float* Y, float* theta, int m);
__global__ void reduceSumOnDevice(float* data, int size);
__global__ void reduceSumOnDevice2(float* data, int size);
//BUGGED - Not finished yet
__global__ void gradientDescentGPU_CUDA(float* J_hist, float* x, float* xWOOne, float* y, float* theta, int m, float alpha, int iter, float* theta_history);
__global__ void gradientDescentGPU_CUDA_Test1(float* J_hist, float* x, float* xWOOne, float* y, float* theta, int m, float alpha, int iter, float* theta_history);
__global__ void gradientDescentGPU_CUDA_Test2(float* J_hist, float* x, float* xWOOne, float* y, float* theta, int m, float alpha, int iter, float* theta_history);
__global__ void gradientDescentGPU_CUDA_Test3(float* J_hist, float* x, float* xWOOne, float* y, float* theta, int m, float alpha, int iter, float* theta_history);
__global__ void gradientDescentGPU_CUDA_Optimized(float* J_hist, float* x, float* xWOOne, float* y, float* theta, int m, float alpha, int iter, float* theta_history, int blockIter);


__device__ float d_sumJDelta1[1];
__device__ float d_sumJDelta2[1];

__device__ float d_Sum[1024];

__device__ __constant__ int d_iter;
__device__ __constant__ float d_alpha;
bool isPowerOfTwo(int n){
	if(n == 0)return 0;

	while(n != 1){

		if(n%2 != 0){
			return 0;
		}

		n /= 2;
	}

	return 1;
}	

int main(int argc, char** argv)
{

	if(argc < 6){
		printf("<Usage> <path/filename> <Number Training Set> <numThread> <Number Iteration for Gradient Descent> <Alpha - Learning Rate, i.e 0.01>\n");
		printf("i.e. costFunction_LinearRegression_CPU_CUDA ex1data16k.txt 1024 32 10 0.01\n");
		return -1;
	}

	clock_t cpu_start, cpu_end;
	clock_t cuda_start, cuda_end;
	//GET data from file 
	char* filename = (char*)malloc(100*sizeof(char));

	sprintf(filename,"%s",argv[1]);
	int numTrainingSet = atoi(argv[2]);
	int numThreadPerBlock = atoi(argv[3]);
	int iter = atoi(argv[4]);
	float alpha = atof(argv[5]); //0.01

	if(!isPowerOfTwo(numTrainingSet)){
		int inc = 1;
		while(inc < numTrainingSet){
			inc = inc << 1;
		}

		inc = inc >> 1;
		numTrainingSet = inc;
	}
	 if(numTrainingSet <= 2){
		return 0;
	}

	printf("numTrainingSet : %d\n", numTrainingSet);

	//%% ======================= Part 2: Get Data =======================
	size_t sizeXY = numTrainingSet*sizeof(float);
	size_t sizeTheta = 2*sizeof(float);
	float* x = (float*)malloc(sizeXY);
	float* y = (float*)malloc(sizeXY);

	int ret = readDataFile(filename, numTrainingSet, x, y);

	x = (float*)realloc(x, sizeXY);
	y = (float*)realloc(y, sizeXY);

	for(int i=0; i < 10; i++)
	{
		printf("X[%d] = %f, Y[%d] = %f\n", i, x[i], i, y[i]);
	}
	//%% =================== Part 3: Cost and Gradient descent ===================
	printf("\n\n============================== CPU REGION =============================================\n");
	float* dataX1 = (float*)malloc(2*sizeXY);
	float* theta= (float*)malloc(sizeTheta);
	for(int i=0; i < 2; i++){
		theta[i] = 0.0;}
	int j = 0;
	for(int i =0; i< 2*numTrainingSet; i++){
		if( i % 2 == 0){
			dataX1[i] = 1.0;
		}else{
			dataX1[i] = x[j];
			j++;
		}
	}
	cpu_start = clock();
	//COMPUTE COST FUNCTION --- J(theta)
	float jCostF = 0.0;
	theta[0] = -1.0;
	theta[1] = 2.0;
	ret = computeCost(jCostF, dataX1, y, theta, numTrainingSet);
	printf("Theta[%f,%f] Result : ComputeCost = %f\n", theta[0], theta[1], jCostF);
	cpu_end = clock();
	double cpu_JCost_Exec_time = (double)(cpu_end - cpu_start)/CLOCKS_PER_SEC;


	//COMPUTE GRADIENT DESCENT --- J(theta)
	size_t sizeHist = iter*sizeof(float);
	float* j_history = (float*)malloc(sizeHist);
	float* theta_history = (float*)malloc(sizeHist*2);
	for(int i=0; i < iter; i++){
		j_history[i] = 0.0;
		theta_history[i] = 0.0;
		theta_history[i+iter] = 0.0;
	}
	for(int i=0; i < 2; i++){
		theta[i] = 0.0;
	}

	size_t memUsage_CPU = sizeXY + sizeXY + (2*sizeXY) + sizeTheta + sizeHist + (sizeHist*2);

	cpu_start = clock();
	printf("Theta[%f,%f] Result : ComputeCost = %f\n", theta[0], theta[1], jCostF);
	ret = gradientDescent(j_history, dataX1, x, y, theta, numTrainingSet, alpha, iter, theta_history);
	printf("Result from Gradient Descent - Theta[%f,%f] Result : ComputeCost = %f\n", theta[0], theta[1], j_history[iter-1]);
	cpu_end = clock();
	double GD_cpu_exec_time = (double)(cpu_end - cpu_start)/CLOCKS_PER_SEC;
	//SET BACK TO ZEROS
	for(int i=0; i < iter; i++){
		j_history[i] = 0.0;
		theta_history[i] = 0.0;
		theta_history[i+iter] = 0.0;
	}
	for(int i=0; i < 2; i++){
		theta[i] = 0.0;
	}

	//============================== CUDA REGION ======================================================
	printf("\n\n============================== CUDA REGION =============================================\n");
	printf("===========COMPUTE COST FUNCTION - LINEAR REGRESSION CUDA===========\n");
	size_t free_byte, total_byte;
	double free_dbyte, total_dbyte, used_dbyte;

	cudaDeviceProp prop;
	checkCudaErrors(cudaGetDeviceProperties(&prop, 0));
	if(prop.canMapHostMemory)
	{
		checkCudaErrors(cudaSetDeviceFlags(cudaDeviceMapHost));
		checkCudaErrors(cudaHostRegister(x, sizeXY, cudaHostAllocMapped));
		checkCudaErrors(cudaHostRegister(y, sizeXY, cudaHostAllocMapped));
		checkCudaErrors(cudaHostRegister(dataX1, 2*sizeXY, cudaHostAllocMapped));
		checkCudaErrors(cudaHostRegister(theta, 2*sizeof(float), cudaHostAllocMapped));
		checkCudaErrors(cudaHostRegister(j_history, sizeHist, cudaHostAllocMapped));
		checkCudaErrors(cudaHostRegister(theta_history, sizeHist*2, cudaHostAllocMapped));
	}else{
		printf("Your computer is not SUPPORTED UNIFIED ADDRESSING\n");
		return -1;
	}
	float* jCostPerBlock;
	checkCudaErrors(cudaHostAlloc((void**)&jCostPerBlock, sizeXY, cudaHostAllocMapped));
	float* hResult;
	checkCudaErrors(cudaHostAlloc((void**)&hResult, sizeof(float), cudaHostAllocMapped));
	float* d_x;
	float* d_y;
	float* d_dataX1;
	float* d_theta;
	float* d_jCostPerBlock;
	float* d_j_history;
	float* d_theta_history;
	checkCudaErrors(cudaHostGetDevicePointer((void **)&d_x, (void*)x, 0));
	checkCudaErrors(cudaHostGetDevicePointer((void **)&d_y, (void*)y, 0));
	checkCudaErrors(cudaHostGetDevicePointer((void **)&d_dataX1, (void*)dataX1, 0));
	checkCudaErrors(cudaHostGetDevicePointer((void **)&d_theta, (void*)theta, 0));
	checkCudaErrors(cudaHostGetDevicePointer((void **)&d_jCostPerBlock, (void*)jCostPerBlock, 0));
	checkCudaErrors(cudaHostGetDevicePointer((void **)&d_j_history, (void*)j_history, 0));
	checkCudaErrors(cudaHostGetDevicePointer((void **)&d_theta_history, (void*)theta_history, 0));

	//ComputeCostFunction on CUDA - Zero Copy
	cuda_start = clock();

	dim3 tpb(numTrainingSet);
	dim3 bpg(1);
	if(numTrainingSet > numThreadPerBlock){
		tpb.x = numThreadPerBlock;
		bpg.x = ceil((double)numTrainingSet/(double)numThreadPerBlock);
	}

	int numShared = tpb.x*2;
	if(tpb.x >= 1024){
		numShared = 4096;
	}

	printf("NumThreads = %d, numBlocks = %d, numTrainingSet = %d\n", tpb.x, bpg.x, numTrainingSet);

	theta[0] = -1.0;
	theta[1] = 2.0;

	hResult[0] = 0.0;

	//SumReduce on Device AND sum Reduce on CPU
	computeCostGPU_CUDA<<<bpg, tpb, 2*tpb.x*sizeof(float)>>>(*hResult, d_jCostPerBlock, d_dataX1, d_y, d_theta, numTrainingSet);
	// computeCostGPU_CUDA<<<bpg, tpb, numShared*sizeof(float)>>>(*hResult, d_dataX1, d_y, d_theta, numTrainingSet);
	checkCudaErrors(cudaPeekAtLastError());
	checkCudaErrors(cudaDeviceSynchronize());

	jCostF = 0.0;
	for(int i = 0; i < numTrainingSet; i++ ){
		// printf("jCostPerBlock[%d] = %f\n", i, jCostPerBlock[i]);
		jCostF += jCostPerBlock[i];
	}
	jCostF = jCostF/(2*numTrainingSet);

	printf("Theta[%f, %f] - Output from ComputeCost GPU CUDA - Zero Copy (NO OPTIMIZED YET) : Result (sumReduce on Device) = %f, Result (compute sum on CPU) = %f\n", theta[0], theta[1], hResult[0], jCostF);

	cuda_end = clock();
	double cuda_zerocopy_exec_time = (double)(cuda_end - cuda_start)/CLOCKS_PER_SEC;

	//ComputeCostFunction_Linear regression on CUDA - Non Zero Copy
	for(int i = 0; i < numTrainingSet; i++ ){
		jCostPerBlock[i] = 0.0;
	}
	hResult[0] = 0.0;
	cudaStream_t streams[3];
	for(int i = 0; i< 3; i++){
		checkCudaErrors(cudaStreamCreate(&streams[i]));
	}

	float* d_dataX2;
	float* d_y2;
	float* d_theta2;
	checkCudaErrors(cudaMalloc((void**)&d_dataX2, 2*sizeXY));
	checkCudaErrors(cudaMalloc((void**)&d_y2, sizeXY));
	checkCudaErrors(cudaMalloc((void**)&d_theta2, 2*sizeof(float)));

	checkCudaErrors(cudaMemcpyAsync(d_dataX2, dataX1, sizeXY*2, cudaMemcpyHostToDevice, streams[0]));
	checkCudaErrors(cudaMemcpyAsync(d_y2, y, sizeXY, cudaMemcpyHostToDevice, streams[1]));
	checkCudaErrors(cudaMemcpyAsync(d_theta2, theta, 2*sizeof(float), cudaMemcpyHostToDevice, streams[0]));

	cuda_start = clock();
	int iter2 = 1;
	dim3 tpb2(numTrainingSet);
	dim3 bpg2(1);
	numShared = tpb2.x;

	if(numTrainingSet < 8192){
		if(numTrainingSet > numThreadPerBlock){

			tpb2.x = numThreadPerBlock;
			numShared = tpb2.x;
			iter2 = ceil(numTrainingSet/(numThreadPerBlock));
		}
		else if(numTrainingSet < numTrainingSet){
			return -1;
		}
	}
	else if(numTrainingSet >= 8192){
		bpg2.x = 4;
		numShared = 8192;
		tpb2.x = numThreadPerBlock;
		iter2 = ceil(numTrainingSet/(numThreadPerBlock*4));
	}
	

	printf("NumThreads = %d, numBlocks = %d, numShared = %d, numTrainingSet = %d, Iteration = %d\n", tpb2.x, bpg2.x, numShared, numTrainingSet, iter2);
	// computeCostGPU_CUDA<<<bpg, tpb, numShared*sizeof(float), streams[2]>>>(*hResult, d_dataX2, d_y2, d_theta, numTrainingSet);
	// computeCostGPU_CUDA<<<bpg, tpb, numShared*sizeof(float), streams[2]>>>(*hResult, d_jcostPerBlock2, d_dataX2, d_y2, d_theta, numTrainingSet);
	// computeCostGPU_CUDA_SMem<<<bpg, tpb, numShared*sizeof(float), streams[2]>>>(*hResult, d_jcostPerBlock2, d_dataX2, d_y2, d_theta2, numTrainingSet);
	// computeCostGPU_CUDA_SMem3<<<bpg, tpb, numShared*sizeof(float), streams[2]>>>(*hResult, d_jcostPerBlock2, d_dataX2, d_y2, d_theta, numTrainingSet, numShared);
	computeCostGPU_CUDA_SMem1b1t<<<bpg2, tpb2, numShared*sizeof(float), streams[0]>>>(*hResult, d_dataX2, d_y2, d_theta2, numTrainingSet, iter2);
	cudaDeviceSynchronize();

	printf("Theta[%f, %f] - Output from ComputeCost GPU CUDA - NON ZERO COPY, Memcpy Async (compute sumReduce on Device) : Result = %f\n", theta[0], theta[1], hResult[0]);

	cuda_end = clock();
	double cuda_computeCost_NONZeroCopy = (double)(cuda_end - cuda_start)/CLOCKS_PER_SEC;


	
	theta[0] = 0.0;
	theta[1] = 0.0;

	float* d_jHist;
	float* d_thetaHist;
	float* d_xWOOne;
	printf("\n\n===========GRADIENT DESCENT CUDA - Not finished yet===========\n");
	printf("jHist = %f,\nx = %f\nxWoone = %f\ny = %f\ntheta[%f, %f]\nm = %d\nalpha = %f,\niter = %d,\nthetaHist = %f\n\n", 
			j_history[0], dataX1[0], x[0], y[0], theta[0], theta[1], numTrainingSet, alpha, iter, theta_history[0]);

	checkCudaErrors(cudaMemcpyToSymbol(d_iter, &iter, sizeof(int), 0, cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpyToSymbol(d_alpha, &alpha, sizeof(float), 0, cudaMemcpyHostToDevice));

	checkCudaErrors(cudaMalloc((void**)&d_jHist, sizeHist));
	checkCudaErrors(cudaMalloc((void**)&d_thetaHist, 2*sizeHist));
	checkCudaErrors(cudaMalloc((void**)&d_xWOOne, sizeXY));

	checkCudaErrors(cudaMemcpyAsync(d_jHist, j_history, sizeHist, cudaMemcpyHostToDevice, streams[0]));
	checkCudaErrors(cudaMemcpyAsync(d_thetaHist, theta_history, 2*sizeHist, cudaMemcpyHostToDevice, streams[0]));
	checkCudaErrors(cudaMemcpyAsync(d_xWOOne, x, sizeXY, cudaMemcpyHostToDevice, streams[1]));
	checkCudaErrors(cudaMemcpyAsync(d_theta2, theta, 2*sizeof(float), cudaMemcpyHostToDevice, 0));


	cuda_start = clock();
	if(numTrainingSet < 8192)
	{ 
		numShared = 4*tpb2.x;
	}else if(numTrainingSet >= 8192){
		bpg2.x = 1;
		iter2 = ceil(numTrainingSet/numThreadPerBlock);
	}

	printf("NumThreads = %d, numBlocks = %d, numShared = %d, numTrainingSet = %d, Iteration = %d\n", tpb2.x, bpg2.x, numShared, numTrainingSet, iter2);

	// gradientDescentGPU_CUDA_Test2<<<bpg, tpb, 4*tpb.x*sizeof(float)>>>(d_jHist, d_dataX2, d_xWOOne, d_y2, d_theta2, numTrainingSet, alpha, iter, d_thetaHist);
	// gradientDescentGPU_CUDA_Test4<<<bpg2, tpb2, numShared*sizeof(float)>>>(d_jHist, d_dataX2, d_xWOOne, d_y2, d_theta2, numTrainingSet, alpha, iter, d_thetaHist, iter2);
	gradientDescentGPU_CUDA_Optimized<<<bpg2, tpb2, numShared*sizeof(float), streams[1]>>>(d_jHist, d_dataX2, d_xWOOne, d_y2, d_theta2, numTrainingSet, alpha, iter, d_thetaHist, iter2);
	checkCudaErrors(cudaPeekAtLastError());
	checkCudaErrors(cudaDeviceSynchronize());

	cuda_end = clock();
	double GD_CUDA_exec_time = (double)(cuda_end - cuda_start)/CLOCKS_PER_SEC;

	checkCudaErrors(cudaMemcpy(j_history, d_jHist, sizeHist, cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(theta_history, d_thetaHist, 2*sizeHist, cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy( theta, d_theta2, 2*sizeof(float), cudaMemcpyDeviceToHost));

	printf("Result from Gradient Descent CUDA - Theta[%f,%f] Result : ComputeCost = %f\n", theta[0], theta[1], j_history[iter-1]);

	for(int i=0, j = 0; i<5; i++, j+=2){
		printf("[i = %d]Theta[%f,%f] Result : gradientDescent -  = %f\n", i, theta_history[j], theta_history[j+1], j_history[i]);
	}

	double memUsage_dcpu = (double) memUsage_CPU;
	printf("Memory Usage CPU - Total = %f MB\n", memUsage_dcpu/(1024*1024));

	checkCudaErrors(cudaMemGetInfo(&free_byte, &total_byte));
	free_dbyte = (double)free_byte;
	total_dbyte = (double)total_byte;
	used_dbyte = total_dbyte - free_dbyte;
	printf("GPU Memory usage : used = %f MB, free = %f MB, total = %f MB\n", used_dbyte/(1024.0*1024.0), free_dbyte/(1024.0*1024.0), total_dbyte/(1024.0*1024.0));

	printf("computeCost CPU HOST Execution Time : %lf\n", cpu_JCost_Exec_time);
	printf("computeCost CUDA Zero copy Execution Time : %lf\n", cuda_zerocopy_exec_time);
	printf("computeCost CUDA NON Zero copy Execution Time : %lf\n", cuda_computeCost_NONZeroCopy);

	printf("Gradient Descent CPU Execution Time : %lf\n", GD_cpu_exec_time);
	printf("Gradient Descent CUDA Execution Time : %lf\n", GD_CUDA_exec_time);


	//CLEAN UP
	//Unregister Host Alloc
	checkCudaErrors(cudaHostUnregister(x));
	checkCudaErrors(cudaHostUnregister(y));
	checkCudaErrors(cudaHostUnregister(theta));
	checkCudaErrors(cudaHostUnregister(dataX1));

	// checkCudaErrors(cudaHostUnregister(hJcost));
	checkCudaErrors(cudaHostUnregister(j_history));
	checkCudaErrors(cudaHostUnregister(theta_history));

	for(int i = 0; i < 3; i++)
		checkCudaErrors(cudaStreamDestroy(streams[i]));

	//FREEING Data Pointer
	checkCudaErrors(cudaFreeHost(jCostPerBlock));
	checkCudaErrors(cudaFreeHost(hResult));

	checkCudaErrors(cudaFree(d_dataX2));
	checkCudaErrors(cudaFree(d_y2));
	checkCudaErrors(cudaFree(d_theta2));
	checkCudaErrors(cudaFree(d_jHist));
	checkCudaErrors(cudaFree(d_thetaHist));
	checkCudaErrors(cudaFree(d_xWOOne));
	free(x);
	free(y);
	free(dataX1);
	free(theta);
	free(theta_history);
	free(j_history);
	return 0;
}

__device__ float tempResult = 0.0;

__global__ void gradientDescentGPU_CUDA_Optimized(float* J_hist, float* x, float* xWOOne, float* y, 
	float* theta, int m, float alpha, int iter, float* theta_history, int blockIter){

	int idx = (blockDim.x*blockIdx.x) + threadIdx.x;
	int tidx = threadIdx.x;
	int bdimx = blockDim.x;
	int gdimx = gridDim.x;
	__shared__ extern float sAllData[];
	float* sSumTemp1 = sAllData;
	float* sSumTemp2 = &sSumTemp1[blockDim.x];

	__shared__ float thetaTemps[2];
	__shared__ float deltaTemps[2];
	float hx = 0.0;
	float jTheta = 0.0;
	int t = 0;
	float x_data;
	float y_data;
	int block = gdimx*bdimx;
	int index = 0;

	if(idx < m){
		thetaTemps[tidx] = 0.0;
		sSumTemp1[tidx] = 0.0;
		sSumTemp2[tidx] = 0.0;
		__syncthreads();
		for(int i = 0; i < iter; i++){
			if(idx == 0){
				theta_history[t] = theta[0];
				theta_history[t+1] = theta[1];
				t++;
			}
			d_sumJDelta1[0] = 0.0;
			d_sumJDelta2[0] = 0.0;

			for(int j = 0; j < blockIter; j++){
				x_data = x[((idx*2)+1)+(block*2*j)];
				y_data = y[(idx)+(block*j)];
				// __syncthreads();
				hx = __fmaf_rn(x_data, theta[1], theta[0] );
				// __syncthreads();
				jTheta = hx - y_data;
				__syncthreads();

				sSumTemp1[tidx] = jTheta;
				sSumTemp2[tidx] = jTheta*x_data;
				__syncthreads();

				for(int s=1; s < bdimx; s*=2){
					index = s*2*tidx;
					if(index < bdimx){
						sSumTemp1[index] += sSumTemp1[index+s];
						sSumTemp2[index] += sSumTemp2[index+s];
					}
				}
				__syncthreads();
				if(tidx == 0){
					atomicAdd(&d_sumJDelta1[0], sSumTemp1[0]);
					atomicAdd(&d_sumJDelta2[0], sSumTemp2[0]);
				}
				__syncthreads();

			}
			deltaTemps[0] = d_sumJDelta1[0]/m;
			deltaTemps[1] = d_sumJDelta2[0]/m;
			__syncthreads();
			thetaTemps[0] = theta[0];
			thetaTemps[1] = theta[1];
			theta[0] = thetaTemps[0] - (d_alpha*deltaTemps[0]);
			theta[1] = thetaTemps[1] - (d_alpha*deltaTemps[1]);
			__syncthreads();
			if(idx == 0){				
				int tpb = blockDim.x;
				int bpg = gridDim.x;
				computeCostGPU_CUDA_SMem1b1t_static<<<bpg, tpb>>>(J_hist[i], x, y, theta, m, blockIter);
				cudaDeviceSynchronize();
			}
		}
	}
}

__global__ void computeCostGPU_CUDA_SMem1b1t_static(float &result, float* X, float* Y, float* theta, int m, int iter){

	int idx = (blockIdx.x * blockDim.x) + threadIdx.x;
	int tidx = threadIdx.x;
	int bdimx = blockDim.x;
	int gdimx = gridDim.x;
	__shared__ float allShared[4096];
	volatile float* sData = allShared;
	float hx, jTheta;
	int index;
	int x_id;
	float x_reg;
	float y_reg;
	int block = gdimx*bdimx;
	if(idx < bdimx*gdimx){
		result = 0.0f;
		sData[tidx] = 0.0f;
		tempResult = 0.0f;
		hx = 0.0f;
		jTheta = 0.0f;
		x_reg = 0.0f;
		y_reg = 0.0f;
		for(int i = 0; i < iter; i++)
		{
			x_id = ((idx*2)+1)+(2*i*block);
			x_reg = X[x_id];
			y_reg = Y[idx+(i*block)];
			// __syncthreads();
			hx = __fmaf_rn(x_reg, theta[1], theta[0]);
			// __syncthreads();
			jTheta = hx - y_reg;
			__syncthreads();
			sData[tidx] = jTheta*jTheta;
			__syncthreads();
			//Do reduction on Shared Memory
				for(int s=1; s<bdimx; s*=2){
					index = 2 * s * tidx;
					if(index < bdimx){
						sData[index] += sData[index+s] ;
					}
				}
			__syncthreads();
			if(tidx == 0){
				atomicAdd(&tempResult, sData[0]);
			}
			__syncthreads();
		}
		result = tempResult/(2*m);
	}
}

__global__ void computeCostGPU_CUDA_SMem1b1t(float &result, float* X, float* Y, float* theta, int m, int iter){

	int idx = (blockIdx.x * blockDim.x) + threadIdx.x;
	int tidx = threadIdx.x;
	int bdimx = blockDim.x;
	int gdimx = gridDim.x;
	__shared__ extern float allShared[];
	volatile float* sData = allShared;
	float hx, jTheta;
	int index;
	int x_id;
	float x_reg;
	float y_reg;
	int block = gdimx*bdimx;
	if(idx < bdimx*gdimx){
		result = 0.0f;
		sData[tidx] = 0.0f;
		tempResult = 0.0f;
		hx = 0.0f;
		jTheta = 0.0f;
		x_reg = 0.0f;
		y_reg = 0.0f;
		for(int i = 0; i < iter; i++)
		{
			x_id = ((idx*2)+1)+(2*i*block);
			x_reg = X[x_id];
			y_reg = Y[idx+(i*block)];
			// __syncthreads();
			hx = __fmaf_rn(x_reg, theta[1], theta[0]);
			// __syncthreads();
			jTheta = hx - y_reg;
			__syncthreads();
			sData[tidx] = jTheta*jTheta;
			__syncthreads();
			//Do reduction on Shared Memory
			for(int s=1; s<bdimx; s*=2){
				index = 2 * s * tidx;
				if(index < bdimx){
					sData[index] += sData[index+s] ;
				}
			}
			__syncthreads();
			if(tidx == 0){
				atomicAdd(&tempResult, sData[0]);
			}
			__syncthreads();
		}
		result = tempResult/(2*m);
	}
}


__global__ void computeCostGPU_CUDA(float &result, float* jCost, float* X, float* Y, float* theta, int m){

	int idx = (blockIdx.x * blockDim.x) + threadIdx.x;
	int tidx = threadIdx.x;
	__shared__ extern float sData[];
	float hx, jTheta;
	int index;
	// if(idx == 0)
	// 	printf("Theta[%f, %f]\n", theta[0], theta[1]);
	if(idx < m){
		sData[tidx] = 0.0;
		tempResult = 0.0;
		hx = 0.0;
		jTheta = 0.0;
		hx = (X[(idx*2)] * theta[0]) + (X[(idx*2)+1] * theta[1]);
		jTheta = hx - Y[idx];
		jCost[idx] = powf(jTheta, 2);
		sData[tidx] = jCost[idx];
		__syncthreads();
		//Do reduction on Shared Memory
		for(int s=1; s<blockDim.x; s*=2){

			index = 2 * s * threadIdx.x;

			if(index < blockDim.x){
				sData[index] += sData[index+s];
			}
			__syncthreads();
		}
		
		//Write result for this block to global memory 
		__syncthreads();
		if(threadIdx.x == 0){
			atomicAdd(&tempResult, sData[0]);
		}
		__syncthreads();

		if(idx == 0){
			result = tempResult/(2*m);
		}
	}
}

__device__ float linearRegressionOneFeature(float x1, float theta1, float x2, float theta2){

	float out = 0.0;
	out = (x1*theta1) + (x2*theta2);
	return out;

}

__device__ float g_sum[1024*1024];

__global__ void reduceSumOnDevice(float* data, int size){
	// int idx = (blockIdx.x * blockIdx.x) + threadIdx.x;
	// int tidx = threadIdx.x;
	int bidx = blockIdx.x;

	if(bidx < size){

		int nTotalThreads = size;
		int half = 0;
		while(nTotalThreads > 1){
			half = nTotalThreads >> 1;
			if(bidx < half){

				data[bidx] += data[bidx + half];
			}
			nTotalThreads = half;
		}
		__syncthreads();
	}
}

__global__ void computeCostGPU_CUDA_SMem3(float &result, float* jCost, float* X, float* Y, float* theta, int m, int numShared){

	int idx = (blockIdx.x * blockDim.x) + threadIdx.x;
	int tidx = threadIdx.x;
	int bidx = blockIdx.x;

	__shared__ extern float sData[];
	__shared__ float sTemp[1];

	g_sum[idx] = 0.0;

	float hx, jTheta;
	int index;

	// if(idx == 0)
	// 	printf("Theta[%f, %f]\n", theta[0], theta[1]);

	if(idx < m){
		sTemp[0] = 0.0;
		sData[tidx] = 0.0;
		tempResult = 0.0;
		hx = 0.0;
		jTheta = 0.0;
		
		for(int i = 0; i < 2; i++){
			sData[tidx] = X[idx*2+i];
			// __syncthreads();
			hx += sData[tidx]*theta[i];
			__syncthreads();
		}
		__syncthreads();

		jTheta = hx - Y[idx];
		jCost[idx] = powf(jTheta, 2);
		sData[tidx] = jCost[idx];
		g_sum[bidx] = 0.0;
		
		__syncthreads();
		//Do reduction on Shared Memory
		for(int s=1; s<blockDim.x; s*=2){
			index = 2 * s * threadIdx.x;
			if(index < blockDim.x){
				sData[index] += sData[index+s];
			}
		}
		__syncthreads();
		//Write result for this block to global memory 
		if(threadIdx.x == 0){
			g_sum[bidx] = sData[0];
			__syncthreads();
		}
		__syncthreads();

		if(tidx == 0 && bidx == 0){
			// printf("3 g_sum[%d] = %f, g_sum[%d] = %f\n", idx, g_sum[tidx], bidx, g_sum[bidx]);
			int gdim = gridDim.x;
			int bpg = gdim;
			int tpb = 1;
			reduceSumOnDevice<<<bpg, tpb>>>(g_sum, gridDim.x);
			cudaDeviceSynchronize();
			// printf("3 AFTER g_sum[%d] = %f, g_sum[%d] = %f\n", idx, g_sum[tidx], bidx, g_sum[bidx]);
		}

		__syncthreads();
		if(idx == 0 && bidx == 0)
		{
			tempResult = g_sum[0];
			// printf("2 result[%d] = %f\n", idx, result);
		}
		__syncthreads();
		if(idx == 0){
			// printf("3 result[%d] = %f\n", idx, result);
			*sTemp = result/(2*m);
			result = *sTemp;
		}
	}
}

__global__ void computeCostGPU_CUDA_SMem2(float &result, float* jCost, float* X, float* Y, float* theta, int m){

	int idx = (blockIdx.x * blockDim.x) + threadIdx.x;
	int tidx = threadIdx.x;
	int bidx = blockIdx.x;
	__shared__ extern float sData[];
	float hx, jTheta;
	int index;
	// if(idx == 0)
	// 	printf("Theta[%f, %f]\n", theta[0], theta[1]);

	if(idx < m){
		g_sum[idx] = 0.0;
		sData[tidx] = 0.0;
		tempResult = 0.0;
		hx = 0.0;
		jTheta = 0.0;
		
		for(int i = 0; i < 2; i++){
			sData[tidx] = X[idx*2+i];
			// __syncthreads();
			hx += sData[tidx]*theta[i];
			__syncthreads();
		}
		__syncthreads();
		jTheta = hx - Y[idx];
		jCost[idx] = powf(jTheta, 2);
		sData[tidx] = jCost[idx];
		g_sum[bidx] = 0.0;
		__syncthreads();
		//Do reduction on Shared Memory
		for(int s=1; s<blockDim.x; s*=2){
			index = 2 * s * threadIdx.x;
			if(index < blockDim.x){
				sData[index] += sData[index+s];
			}
		}
		__syncthreads();
		//Write result for this block to global memory 
		if(threadIdx.x == 0){
			g_sum[bidx] = sData[0];
		}
		__syncthreads();

		if(tidx == 0 && bidx == 0){
			// printf("3 g_sum[%d] = %f, g_sum[%d] = %f\n", idx, g_sum[tidx], bidx, g_sum[bidx]);
			int gdim = gridDim.x;
			int bpg = gdim;
			int tpb = 1;
			reduceSumOnDevice<<<bpg, tpb>>>(g_sum, gridDim.x);
			cudaDeviceSynchronize();
			// printf("3 AFTER g_sum[%d] = %f, g_sum[%d] = %f\n", idx, g_sum[tidx], bidx, g_sum[bidx]);
		}
		__syncthreads();
		if(idx == 0 && bidx == 0)
		{
			result = g_sum[0];
			// printf("2 result[%d] = %f\n", idx, result);
		}
		__syncthreads();
		if(idx == 0){
			// printf("3 result[%d] = %f\n", idx, result);
			result = result/(2*m);
		}
	}
}

__global__ void computeCostGPU_CUDA_SMem(float &result, float* jCost, float* X, float* Y, float* theta, int m){

	int idx = (blockIdx.x * blockDim.x) + threadIdx.x;
	int tidx = threadIdx.x;
	__shared__ extern float sData[];
	float hx, jTheta;
	int index;
	// if(idx == 0)
	// 	printf("Theta[%f, %f]\n", theta[0], theta[1]);
	if(idx < m){
		sData[tidx] = 0.0;
		tempResult = 0.0;
		hx = 0.0;
		jTheta = 0.0;

		for(int i = 0; i < 2; i++){
			sData[tidx] = X[idx*2+i];
			__syncthreads();
			hx += sData[tidx]*theta[i];
			__syncthreads();
		}
		jTheta = hx - Y[idx];
		jCost[idx] = powf(jTheta, 2);
		sData[tidx] = jCost[idx];
		__syncthreads();
		//Do reduction on Shared Memory
		for(int s=1; s<blockDim.x; s*=2){
			index = 2 * s * threadIdx.x;
			if(index < blockDim.x){
				sData[index] += sData[index+s];
			}
		}
		__syncthreads();
		//Write result for this block to global memory 
		if(threadIdx.x == 0){
			atomicAdd(&tempResult, sData[0]);
		}
		__syncthreads();
		if(idx == 0){
			result = tempResult/(2*m);
		}
	}
}

__global__ void reduceSumOnDevice2(float* data, int size){
	int idx = (blockIdx.x * blockIdx.x) + threadIdx.x;
	if(idx < size){

		int nTotalThreads = size;
		int half = 0;
		while(nTotalThreads > 1){
			half = nTotalThreads >> 1;
			if(idx < half){

				data[idx] += data[idx + half];
			}
			nTotalThreads = half;
		}
		__syncthreads();
	}
}

__global__ void computeCostGPU_CUDA(float &result, float* X, float* Y, float* theta, int m){

	int idx = (blockIdx.x * blockDim.x) + threadIdx.x;
	float hx, jTheta;
	// if(idx == 0)
	// 	printf("Theta[%f, %f]\n", theta[0], theta[1]);
	if(idx < m){
		tempResult = 0.0;
		hx = 0.0;
		jTheta = 0.0;
		hx = (X[(idx*2)] * theta[0]) + (X[(idx*2)+1] * theta[1]);
		jTheta = hx - Y[idx];
		d_Sum[idx] = powf(jTheta, 2);
		__syncthreads();
		//Write result for this block to global memory 
		if(idx == 0){
			int tpb = blockDim.x;
			int bpg = gridDim.x;
			reduceSumOnDevice2<<<bpg, tpb>>>(d_Sum, m);
			cudaDeviceSynchronize();
		}
		__syncthreads();
		if(idx == 0){
			result = d_Sum[0]/(2*m);
		}
	}
}

__device__ float d_Sum1D[1024];
__device__ float d_Sum2D[1024];
__global__ void gradientDescentGPU_CUDA_Test3(float* J_hist, float* x, float* xWOOne, float* y, 
	float* theta, int m, float alpha, int iter, float* theta_history){

	int idx = (blockDim.x*blockIdx.x) + threadIdx.x;
	int tidx = threadIdx.x;

	__shared__ extern float sAllData[];
	float* sData = sAllData;
	// float* sTemp1 = &sData[blockDim.x];
	// float* sTemp2 = &sTemp1[blockDim.x];
	// volatile float* sTempY = &sData[blockDim.x];
	float* sSumTemp1 = &sData[blockDim.x];
	float* sSumTemp2 = &sSumTemp1[blockDim.x];

	__shared__ float thetaTemps[2];
	__shared__ float deltaTemps[2];
	float hx = 0.0;
	float jTheta = 0.0;
	int t = 0;
	float dataX = 0.0;
	float dataY = 0.0;
	

	if(idx < m){
		sData[tidx] = 0.0;
		// sTemp1[tidx] = 0.0;
		// sTemp2[tidx] = 0.0;
		// sTempY[tidx] = 0.0;
		thetaTemps[tidx] = 0.0;
		sSumTemp1[tidx] = 0.0;
		sSumTemp2[tidx] = 0.0;
		__syncthreads();
		float theta0 = theta[0];
		float theta1 = theta[1];
		// sData[tidx] = x[(idx*2)+1];
		dataX = x[(idx*2)+1];
		// sTemp1[tidx] = x[idx*2];
		// // __syncthreads();
		// sTemp2[tidx] = x[(idx*2)+1];
		// __syncthreads();
		// sTempY[tidx] = y[idx];
		dataY = y[idx];
		__syncthreads();
		for(int i = 0; i < d_iter; i++){
			// if(idx == 0){
				theta_history[t] = theta[0];
				theta_history[t+1] = theta[1];
				t++;
			// }
			d_sumJDelta1[0] = 0.0;
			d_sumJDelta2[0] = 0.0;
			// printf("1 idx[%d] - sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n",idx, tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			// printf("1 idx[%d] - x[%d] == %f, x[%d] = %f\n",idx, idx*2, x[idx*2], (idx*2)+1, x[(idx*2)+1]);

			__syncthreads();
			// hx = linearRegressionOneFeature(x[idx*2], theta[0], x[(idx*2)+1], theta[1]);
			hx = linearRegressionOneFeature(1, theta0, dataX, theta1);
			// if(idx == 0){
				// printf("hx == %f\n", hx);
			// }
			__syncthreads();

			// printf("2 idx[%d] - sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n",idx, tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			// printf("2 idx[%d] - x[%d] == %f, x[%d] = %f\n",idx, idx*2, x[idx*2], (idx*2)+1, x[(idx*2)+1]);

			// return;
			// jTheta = hx - y[idx];
			jTheta = hx - dataY;
			// if(idx == 0){
				// printf("jTheta == %f\n", jTheta);
			// }
			__syncthreads();

			sSumTemp1[tidx] = jTheta;
			sSumTemp2[tidx] = jTheta*dataX;
			// if(idx == 0){
				// printf("1 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			// }
			__syncthreads();

			int nTotalThreads = m;
			int half = 0;
			while(nTotalThreads > 1){
				half = nTotalThreads >> 1;
				if(tidx < half){
					sSumTemp1[tidx] += sSumTemp1[tidx+half];
					sSumTemp2[tidx] += sSumTemp2[tidx+half];
					// printf("2 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
				}
				nTotalThreads = half;
				// __syncthreads();
			}
			// if(idx == 0)
				// printf("3 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			__syncthreads();
			if(tidx == 0){
				atomicAdd(&d_sumJDelta1[0], sSumTemp1[0]);
				atomicAdd(&d_sumJDelta2[0], sSumTemp2[0]);
				// printf("4 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			}

			// __syncthreads();

			__syncthreads();
			if(idx == 0){
				deltaTemps[0] = d_sumJDelta1[0]/m;
				deltaTemps[1] = d_sumJDelta2[0]/m;
			}
			__syncthreads();
			if(idx == 0){
				thetaTemps[0] = theta[0];
				thetaTemps[1] = theta[1];
				theta[0] = thetaTemps[0] - (d_alpha*deltaTemps[0]);
				theta[1] = thetaTemps[1] - (d_alpha*deltaTemps[1]);
			}
			__syncthreads();
			if(idx == 0){
				// printf("Before Theta[%f, %f] - jCost[%d] = %f\n", theta[0], theta[1], i, J_hist[i]);
				int tpb = blockDim.x;
				int bpg = gridDim.x;
				computeCostGPU_CUDA<<<bpg, tpb>>>(J_hist[i], x, y, theta, m);
				cudaDeviceSynchronize();
				// printf("After Theta[%f, %f] - jCost[%d] = %f\n", theta[0], theta[1], i, J_hist[i]);
			}
			// __syncthreads();
		}
	}
}


__global__ void gradientDescentGPU_CUDA_Test2(float* J_hist, float* x, float* xWOOne, float* y, 
	float* theta, int m, float alpha, int iter, float* theta_history){

	int idx = (blockDim.x*blockIdx.x) + threadIdx.x;
	int tidx = threadIdx.x;
	// int bidx = blockIdx.x;

	// if(idx == 0){
	// 	printf("on Device - jHist = %f,\nx = %f\nxWoone = %f\ny = %f\ntheta[%f, %f]\nm = %d\nalpha = %f,\niter = %d,\nthetaHist = %f\n\n", 
	// 		J_hist[0], x[0], xWOOne[0], y[0], theta[0], theta[1], m, alpha, iter, theta_history[0]);
	// 	printf("on Device 2- jHist = %f,\nx = %f\nxWoone = %f\ny = %f\ntheta[%f, %f]\nm = %d\nd_alpha = %f,\nd_iter = %d,\nthetaHist = %f\n\n", 
	// 		J_hist[0], x[0], xWOOne[0], y[0], theta[0], theta[1], m, d_alpha, d_iter, theta_history[0]);
	// }

	__shared__ extern float sAllData[];
	volatile float* sData = sAllData;
	// float* sTemp1 = &sData[blockDim.x];
	// float* sTemp2 = &sTemp1[blockDim.x];
	volatile float* sTempY = &sData[blockDim.x];
	volatile  float* sSumTemp1 = &sTempY[blockDim.x];
	volatile float* sSumTemp2 = &sSumTemp1[blockDim.x];

	__shared__ float thetaTemps[2];
	__shared__ float deltaTemps[2];
	float hx = 0.0;
	float jTheta = 0.0;
	int t = 0;

	if(idx < m){
		sData[tidx] = 0.0;
		// sTemp1[tidx] = 0.0;
		// sTemp2[tidx] = 0.0;
		sTempY[tidx] = 0.0;
		thetaTemps[tidx] = 0.0;
		sSumTemp1[tidx] = 0.0;
		sSumTemp2[tidx] = 0.0;
		__syncthreads();

		sData[tidx] = x[(idx*2)+1];
		// sTemp1[tidx] = x[idx*2];
		// // __syncthreads();
		// sTemp2[tidx] = x[(idx*2)+1];
		// __syncthreads();
		sTempY[tidx] = y[idx];
		__syncthreads();
		for(int i = 0; i < d_iter; i++){
			// if(idx == 0){
				theta_history[t] = theta[0];
				theta_history[t+1] = theta[1];
				t++;
			// }
			d_sumJDelta1[0] = 0.0;
			d_sumJDelta2[0] = 0.0;
			// printf("1 idx[%d] - sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n",idx, tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			// printf("1 idx[%d] - x[%d] == %f, x[%d] = %f\n",idx, idx*2, x[idx*2], (idx*2)+1, x[(idx*2)+1]);

			__syncthreads();
			// hx = linearRegressionOneFeature(x[idx*2], theta[0], x[(idx*2)+1], theta[1]);
			hx = linearRegressionOneFeature(1, theta[0], sData[tidx], theta[1]);
			// if(idx == 0){
				// printf("hx == %f\n", hx);
			// }
			__syncthreads();

			// printf("2 idx[%d] - sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n",idx, tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			// printf("2 idx[%d] - x[%d] == %f, x[%d] = %f\n",idx, idx*2, x[idx*2], (idx*2)+1, x[(idx*2)+1]);

			// return;
			// jTheta = hx - y[idx];
			jTheta = hx - sTempY[tidx];
			// if(idx == 0){
				// printf("jTheta == %f\n", jTheta);
			// }
			__syncthreads();

			sSumTemp1[tidx] = jTheta;
			sSumTemp2[tidx] = jTheta*sData[tidx];
			// sSumTemp2[tidx] = jTheta*xWOOne[idx];
			// if(idx == 0){
				// printf("1 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			// }
			__syncthreads();

			int nTotalThreads = m;
			int half = 0;
			while(nTotalThreads > 1){
				half = nTotalThreads >> 1;
				if(tidx < half){
					sSumTemp1[tidx] += sSumTemp1[tidx+half];
					sSumTemp2[tidx] += sSumTemp2[tidx+half];
					// printf("2 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
				}
				nTotalThreads = half;
				__syncthreads();
			}
			// if(idx == 0)
				// printf("3 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			__syncthreads();
			if(tidx == 0){
				atomicAdd(&d_sumJDelta1[0], sSumTemp1[0]);
				atomicAdd(&d_sumJDelta2[0], sSumTemp2[0]);
				// printf("4 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			}

			__syncthreads();
			if(idx == 0){
				deltaTemps[0] = d_sumJDelta1[0]/m;
				deltaTemps[1] = d_sumJDelta2[0]/m;
			// }
			__syncthreads();
			// if(idx == 0){
				thetaTemps[0] = theta[0];
				thetaTemps[1] = theta[1];
				theta[0] = thetaTemps[0] - (d_alpha*deltaTemps[0]);
				theta[1] = thetaTemps[1] - (d_alpha*deltaTemps[1]);
			}
			__syncthreads();
			if(idx == 0){
				// printf("Before Theta[%f, %f] - jCost[%d] = %f\n", theta[0], theta[1], i, J_hist[i]);
				int tpb = blockDim.x;
				int bpg = gridDim.x;
				computeCostGPU_CUDA<<<bpg, tpb>>>(J_hist[i], x, y, theta, m);
				cudaDeviceSynchronize();
				// printf("After Theta[%f, %f] - jCost[%d] = %f\n", theta[0], theta[1], i, J_hist[i]);
			}
			// __syncthreads();
		}
	}
}

__global__ void gradientDescentGPU_CUDA_Test1(float* J_hist, float* x, float* xWOOne, float* y, 
	float* theta, int m, float alpha, int iter, float* theta_history){

	int idx = (blockDim.x*blockIdx.x) + threadIdx.x;
	int tidx = threadIdx.x;
	// int bidx = blockIdx.x;

	// if(idx == 0){
	// 	printf("on Device - jHist = %f,\nx = %f\nxWoone = %f\ny = %f\ntheta[%f, %f]\nm = %d\nalpha = %f,\niter = %d,\nthetaHist = %f\n\n", 
	// 		J_hist[0], x[0], xWOOne[0], y[0], theta[0], theta[1], m, alpha, iter, theta_history[0]);
	// 	printf("on Device 2- jHist = %f,\nx = %f\nxWoone = %f\ny = %f\ntheta[%f, %f]\nm = %d\nd_alpha = %f,\nd_iter = %d,\nthetaHist = %f\n\n", 
	// 		J_hist[0], x[0], xWOOne[0], y[0], theta[0], theta[1], m, d_alpha, d_iter, theta_history[0]);
	// }

	__shared__ extern float sAllData[];
	float* sData = sAllData;
	// float* sTemp1 = &sData[blockDim.x];
	// float* sTemp2 = &sTemp1[blockDim.x];
	// float* sTempY = &sTemp2[blockDim.x];
	float* sSumTemp1 = &sData[blockDim.x];
	float* sSumTemp2 = &sSumTemp1[blockDim.x];

	__shared__ float thetaTemps[2];
	__shared__ float deltaTemps[2];
	float hx = 0.0;
	float jTheta = 0.0;
	int t = 0;

	if(idx < m){
		sData[tidx] = 0.0;
		// sTemp1[tidx] = 0.0;
		// sTemp2[tidx] = 0.0;
		// sTempY[tidx] = 0.0;
		thetaTemps[tidx] = 0.0;
		sSumTemp1[tidx] = 0.0;
		sSumTemp2[tidx] = 0.0;
		__syncthreads();

		// sTemp1[tidx] = x[idx*2];
		// // __syncthreads();
		// sTemp2[tidx] = x[(idx*2)+1];
		// __syncthreads();
		// sTempY[tidx] = y[idx];
		// __syncthreads();
		for(int i = 0; i < d_iter; i++){
			if(idx == 0){
				theta_history[t] = theta[0];
				theta_history[t+1] = theta[1];
				t++;
			}
			d_sumJDelta1[0] = 0.0;
			d_sumJDelta2[0] = 0.0;
			// printf("1 idx[%d] - sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n",idx, tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			// printf("1 idx[%d] - x[%d] == %f, x[%d] = %f\n",idx, idx*2, x[idx*2], (idx*2)+1, x[(idx*2)+1]);

			__syncthreads();
			// hx = linearRegressionOneFeature(x[idx*2], theta[0], x[(idx*2)+1], theta[1]);
			hx = linearRegressionOneFeature(1, theta[0], x[(idx*2)+1], theta[1]);
			// if(idx == 0){
				// printf("hx == %f\n", hx);
			// }
			__syncthreads();

			// printf("2 idx[%d] - sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n",idx, tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			// printf("2 idx[%d] - x[%d] == %f, x[%d] = %f\n",idx, idx*2, x[idx*2], (idx*2)+1, x[(idx*2)+1]);

			// return;
			jTheta = hx - y[idx];
			// if(idx == 0){
				// printf("jTheta == %f\n", jTheta);
			// }
			__syncthreads();

			sSumTemp1[tidx] = jTheta;
			sSumTemp2[tidx] = jTheta*xWOOne[idx];
			// if(idx == 0){
				// printf("1 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			// }
			__syncthreads();

			int nTotalThreads = m;
			int half = 0;
			while(nTotalThreads > 1){
				half = nTotalThreads >> 1;
				if(tidx < half){
					sSumTemp1[tidx] += sSumTemp1[tidx+half];
					sSumTemp2[tidx] += sSumTemp2[tidx+half];
					// printf("2 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
				}
				nTotalThreads = half;
				__syncthreads();
			}
			// if(idx == 0)
				// printf("3 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			__syncthreads();
			if(tidx == 0){
				atomicAdd(&d_sumJDelta1[0], sSumTemp1[0]);
				atomicAdd(&d_sumJDelta2[0], sSumTemp2[0]);
				// printf("4 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			}

			__syncthreads();
			if(idx == 0){
				deltaTemps[0] = d_sumJDelta1[0]/m;
				deltaTemps[1] = d_sumJDelta2[0]/m;
			}
			__syncthreads();
			if(idx == 0){
				thetaTemps[0] = theta[0];
				thetaTemps[1] = theta[1];
				theta[0] = thetaTemps[0] - (d_alpha*deltaTemps[0]);
				theta[1] = thetaTemps[1] - (d_alpha*deltaTemps[1]);
			}
			__syncthreads();
			if(idx == 0){
				// printf("Before Theta[%f, %f] - jCost[%d] = %f\n", theta[0], theta[1], i, J_hist[i]);
				int tpb = blockDim.x;
				int bpg = gridDim.x;
				computeCostGPU_CUDA<<<bpg, tpb>>>(J_hist[i], x, y, theta, m);
				cudaDeviceSynchronize();
				// printf("After Theta[%f, %f] - jCost[%d] = %f\n", theta[0], theta[1], i, J_hist[i]);
			}
			__syncthreads();
		}
	}
}

//BUGGED - On Progress
__global__ void gradientDescentGPU_CUDA(float* J_hist, float* x, float* xWOOne, float* y, 
	float* theta, int m, float alpha, int iter, float* theta_history){

	int idx = (blockDim.x*blockIdx.x) + threadIdx.x;
	int tidx = threadIdx.x;
	// int bidx = blockIdx.x;

	// if(idx == 0){
	// 	printf("on Device - jHist = %f,\nx = %f\nxWoone = %f\ny = %f\ntheta[%f, %f]\nm = %d\nalpha = %f,\niter = %d,\nthetaHist = %f\n\n", 
	// 		J_hist[0], x[0], xWOOne[0], y[0], theta[0], theta[1], m, alpha, iter, theta_history[0]);
	// 	printf("on Device 2- jHist = %f,\nx = %f\nxWoone = %f\ny = %f\ntheta[%f, %f]\nm = %d\nd_alpha = %f,\nd_iter = %d,\nthetaHist = %f\n\n", 
	// 		J_hist[0], x[0], xWOOne[0], y[0], theta[0], theta[1], m, d_alpha, d_iter, theta_history[0]);
	// }

	__shared__ extern float sAllData[];
	float* sData = sAllData;
	float* sTemp1 = &sData[blockDim.x];
	float* sTemp2 = &sTemp1[blockDim.x];
	float* sTempY = &sTemp2[blockDim.x];
	float* sSumTemp1 = &sTempY[blockDim.x];
	float* sSumTemp2 = &sSumTemp1[blockDim.x];

	__shared__ float thetaTemps[2];
	__shared__ float deltaTemps[2];
	float hx = 0.0;
	float jTheta = 0.0;
	int t = 0;

	if(idx < m){
		sData[tidx] = 0.0;
		sTemp1[tidx] = 0.0;
		sTemp2[tidx] = 0.0;
		sTempY[tidx] = 0.0;
		thetaTemps[tidx] = 0.0;
		sSumTemp1[tidx] = 0.0;
		sSumTemp2[tidx] = 0.0;
		__syncthreads();

		sTemp1[tidx] = x[idx*2];
		// __syncthreads();
		sTemp2[tidx] = x[(idx*2)+1];
		__syncthreads();
		sTempY[tidx] = y[idx];
		__syncthreads();
		for(int i = 0; i < d_iter; i++){
			if(idx == 0){
				theta_history[t] = theta[0];
				theta_history[t+1] = theta[1];
				t++;
			}
			printf("1 idx[%d] - sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n",idx, tidx, sSumTemp1[tidx], sSumTemp2[tidx]);

			__syncthreads();
			hx = linearRegressionOneFeature(sTemp1[tidx], theta[0], sTemp2[tidx], theta[1]);
			if(idx == 0){
				// printf("hx == %f\n", hx);
			}
			__syncthreads();

			printf("2 idx[%d] - sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n",idx, tidx, sSumTemp1[tidx], sSumTemp2[tidx]);

			// return;
			jTheta = hx - sTempY[tidx];
			if(idx == 0){
				// printf("jTheta == %f\n", jTheta);
			}
			__syncthreads();

			sSumTemp1[tidx] = jTheta;
			sSumTemp2[tidx] = jTheta*sTempY[tidx];
			if(idx == 0){
				// printf("1 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			}
			__syncthreads();

			int nTotalThreads = m;
			int half = 0;
			while(nTotalThreads > 1){
				half = nTotalThreads >> 1;
				if(tidx < half){
					sSumTemp1[tidx] += sSumTemp1[tidx+half];
					sSumTemp2[tidx] += sSumTemp2[tidx+half];
					// printf("2 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
				}
				nTotalThreads = half;
				__syncthreads();
			}
			if(idx == 0)
				// printf("3 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			__syncthreads();
			if(tidx == 0){
				atomicAdd(&d_sumJDelta1[0], sSumTemp1[0]);
				atomicAdd(&d_sumJDelta2[0], sSumTemp2[0]);
				// printf("4 sSumTemp1[%d] == %f, sSumTemp1[tidx] = %f\n", tidx, sSumTemp1[tidx], sSumTemp2[tidx]);
			}
			if(idx == 0){
				deltaTemps[0] = d_sumJDelta1[0]/m;
				deltaTemps[1] = d_sumJDelta2[0]/m;
			}
			__syncthreads();
			if(idx == 0){
				thetaTemps[0] = theta[0];
				thetaTemps[1] = theta[1];
				theta[0] = thetaTemps[0] - (d_alpha*deltaTemps[0]);
				theta[1] = thetaTemps[1] - (d_alpha*deltaTemps[1]);
			}
			__syncthreads();
			if(idx == 0){
				int tpb = blockDim.x;
				int bpg = gridDim.x;
				computeCostGPU_CUDA<<<bpg, tpb>>>(J_hist[i], x, y, theta, m);
				cudaDeviceSynchronize();
			}
			__syncthreads();
		}
	}
}



int readDataFile(char* filen, int num, float* x, float* y)
{
	FILE* fdata;
	int numLines = 0;
	int i = 0;
	fdata = fopen(filen, "r");
	if(fdata == NULL){
		perror("Failed to read file. Check the name of input file");
		return 1;
	}
	double xtemp =0.00;
	double ytemp = 0.00;

	while(EOF != fscanf(fdata,"%lf,%lf\n",&xtemp,&ytemp)){
		if(i < num){
			x[i] = (float) xtemp;
			y[i] = (float) ytemp;
		}
		i++;
		numLines++;
	}
	printf("Number Lines : %d\n", numLines);
	fclose(fdata);
	return 0;
}

int computeCost(float &out, float* x, float* y, float* theta, int m){
	float hx = 0.0;
	float jTheta = 0.0;
	float sumJ = 0.0;
	for(int j = 0, k = 0; j < m; j++, k+=2){
		hx = (x[k]*theta[0]) + (x[k+1] * theta[1]);
		jTheta = hx - y[j];
		sumJ += powf(jTheta,2);
	}
	// printf("CPU SumJ -> = %f\n", sumJ);
	out = sumJ/(2*m);
	// printf("CPU ouT -> = %f\n", out);
	return 0;
}

int gradientDescent(float* J_hist, float* x, float* xWOOne, float* y, 
	float* theta, int m, float alpha, int iter, float* theta_history){
	float delta1 = 0.0;
	float delta2 = 0.0;
	float hx = 0.0;
	float jTheta = 0.0;
	float sumJDelta1 = 0.0;
	float sumJDelta2 = 0.0;
	int ret = 0;
	int t = 0;
	float tempT1 = 0.0;
	float tempT2 = 0.0;

	for(int i=0; i<iter ;i++){
			theta_history[t] = theta[0];
			theta_history[t+1] = theta[1];
			t+=2;
			sumJDelta1 = 0.0;
			sumJDelta2 = 0.0;
			for(int j = 0, k = 0; j < m; j++, k+=2){
			hx = (x[k]*theta[0]) + (x[k+1] * theta[1]);
			jTheta = hx - y[j];
			sumJDelta1 += jTheta;
			sumJDelta2 += (jTheta*xWOOne[j]);
		}
		delta1 = sumJDelta1/m;
		delta2 = sumJDelta2/m;
		tempT1 = theta[0];
		tempT2 = theta[1];
		theta[0] = tempT1 - (alpha*delta1);
		theta[1] = tempT2 - (alpha*delta2);
		ret = computeCost(J_hist[i], x, y, theta, m);
		// printf("After Theta[%f, %f] - jCost[%d] = %f\n", theta[0], theta[1], i, J_hist[i]);
	}
	return ret;
}