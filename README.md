# Linear Regression (Cost Function & Gradient Descent) - Optimized

![Comparison Computation of Cost Function - Linear Regression on CPU, CUDA, & OpenCL](https://bytebucket.org/MuhammadRizky/linearregression_cpu_cuda_opencl_optimized/raw/8c20b952f0027e2cf4cc496217e89e7195f5f605/images/comparsion_CostF.png)


![Comparison Computation of Gradient Descent - Linear Regression on CPU, CUDA, & OpenCL](https://bytebucket.org/MuhammadRizky/linearregression_cpu_cuda_opencl_optimized/raw/8c20b952f0027e2cf4cc496217e89e7195f5f605/images/comparison_GradDescent.png)

## Installation

Requirements : CUDA SDK

## Usage

<Usage> <path/filename> <Number Training Set> <numThread> <Number Iteration for Gradient Descent> <Alpha - Learning Rate, i.e 0.01>

i.e. 
`costFunction_LinearRegression_CPU_CUDA ex1data16k.txt 1024 32 10 0.01`

Reproduce to get the above images for CPU and CUDA comparison:

	1. LinearRegression_CPU_CUDA ../ex1data1.txt 1024 1024 100 0.01
	2. LinearRegression_CPU_CUDA ../ex1data1m.txt 16384 1024 100 0.01
	3. LinearRegression_CPU_CUDA ../ex1data1m.txt 32768 1024 100 0.01
	4. LinearRegression_CPU_CUDA ../ex1data1m.txt 1048576 1024 100 0.01
	5. LinearRegression_CPU_CUDA ../ex1data32m.txt 8388608 1024 100 0.01
	6. LinearRegression_CPU_CUDA ../ex1data32m.txt 33554432 1024 100 0.01
	7. LinearRegression_CPU_CUDA ../ex1data128m.txt 67108864 1024 100 0.01

Reproduce to get the above images for CPU and OpenCL comparison:

	1. LinearRegression_CPU_OPENCL ../ex1data1.txt 1024 1024 100 0.01
	2. LinearRegression_CPU_OPENCL ../ex1data1m.txt 16384 1024 100 0.01
	3. LinearRegression_CPU_OPENCL ../ex1data1m.txt 32768 1024 100 0.01
	4. LinearRegression_CPU_OPENCL ../ex1data1m.txt 1048576 1024 100 0.01
	5. LinearRegression_CPU_OPENCL ../ex1data32m.txt 8388608 1024 100 0.01
	6. LinearRegression_CPU_OPENCL ../ex1data32m.txt 33554432 1024 100 0.01
	7. LinearRegression_CPU_OPENCL ../ex1data128m.txt 67108864 1024 100 0.01


## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

	April, 11 2018 : Image of comparison computation published
	April, 12 2018 : Gradient Descent Optimized
	April, 15 2018 : Added Linear Regression (Cost Function and Gradient Descent) OPENCL Optimized

## Credits

Muhammad Rizky Hatsa

## Cite

If you use this source code in your research please cite it:

```
@misc{linregRiz18,
  author =   {Muhammad Rizky Hatsa},
  title =    {Optimized Linear Regression in CUDA and OPENCL Framework},
  howpublished = {\url{https://bitbucket.org/MuhammadRizky/linearregression_cpu_cuda_opencl_optimized}},
  year = {2018}
}
```