


//Ref_atomic_add_global : http://simpleopencl.blogspot.co.id/2013/05/atomic-operations-and-floats-in-opencl.html
void atomic_add_global(volatile global float *source, const float operand) {
    union {
        unsigned int intVal;
        float floatVal;
    } newVal;
    union {
        unsigned int intVal;
        float floatVal;
    } prevVal;
 
    do {
        prevVal.floatVal = *source;
        newVal.floatVal = prevVal.floatVal + operand;
    } while (atomic_cmpxchg((volatile global unsigned int *)source, prevVal.intVal, newVal.intVal) != prevVal.intVal);
}

__global float tempResult;

__kernel void computeCostGPU_CUDA_SMem1b1t_static(__global float *result, __global float* X, __global float* Y, __global float* theta, const int m, const int iter)
{
	int idx = get_global_id(0);
	int tidx = get_local_id(0);
	int bdimx = get_local_size(0);
	int gdimx = get_num_groups(0);
	int block = get_global_size(0);

	__local float sData[4096];

	float hx;
	float jTheta;
	int index;
	int x_id;
	float x_reg;
	float y_reg;

	if(idx < block)
	{
		*result = 0.0f;
		sData[tidx] = 0.0f;
		tempResult = 0.0f;
		index = 0;
		hx = 0.0f;
		jTheta = 0.0f;
		x_reg = 0.0f;
		y_reg = 0.0f;
		for(int i = 0; i < iter; i++)
		{
			x_id = ((idx*2)+1)+(2*i*block);
			x_reg = X[x_id];
			y_reg = Y[idx+(i*block)];

			hx = theta[0] + (x_reg * theta[1]);
			jTheta = hx - y_reg;

			barrier(CLK_LOCAL_MEM_FENCE);
			
			sData[tidx] = jTheta*jTheta;
			work_group_barrier(CLK_LOCAL_MEM_FENCE);
			for(int s = 1; s < bdimx; s*=2)
			{
				index = 2*s*tidx;
				if(index < bdimx)
				{
					sData[index] += sData[index+s];
				}
			}
			//barrier(CLK_LOCAL_MEM_FENCE);
			work_group_barrier(CLK_LOCAL_MEM_FENCE);
			if(tidx==0)
			{
				//atomic_fetch_add(&tempResult, sData[0]);
				atomic_add_global(&tempResult, sData[0]);
				//atomic_add_local(&tempResult[0], sData[0]);
				//atomicAdd(&tempResult[0], sData[0]);


			}
			work_group_barrier(CLK_LOCAL_MEM_FENCE);
			
		}
	*result = tempResult/(2*m);
	barrier(CLK_LOCAL_MEM_FENCE);
	}
}

__kernel void computeCostGPU_CUDA_SMem1b1t_static_test(float *result, __global float* X, __global float* Y, __global float* theta, const int m, const int iter)
{
	int idx = get_global_id(0);
	int tidx = get_local_id(0);
	int bdimx = get_local_size(0);
	int gdimx = get_num_groups(0);
	int block = get_global_size(0);

	__local float sData[4096];
	float hx;
	float jTheta;
	int index;
	int x_id;
	float x_reg;
	float y_reg;

	if(idx < block)
	{
		sData[tidx] = 0.0f;
		tempResult = 0.0f;
		index = 0;
		hx = 0.0f;
		jTheta = 0.0f;
		x_reg = 0.0f;
		y_reg = 0.0f;
		for(int i = 0; i < iter; i++)
		{
			x_id = ((idx*2)+1)+(2*i*block);
			x_reg = X[x_id];
			y_reg = Y[idx+(i*block)];
			hx = theta[0] + (x_reg * theta[1]);
			jTheta = hx - y_reg;
			barrier(CLK_LOCAL_MEM_FENCE);
			sData[tidx] = jTheta*jTheta;
			work_group_barrier(CLK_LOCAL_MEM_FENCE);
			for(int s = 1; s < bdimx; s*=2)
			{
				index = 2*s*tidx;
				if(index < bdimx)
				{
					sData[index] += sData[index+s];
				}
			}
			work_group_barrier(CLK_LOCAL_MEM_FENCE);
			if(tidx==0)
			{
				atomic_add_global(&tempResult, sData[0]);
			}
			work_group_barrier(CLK_LOCAL_MEM_FENCE);
		}
	*result = tempResult/(2*m);
	barrier(CLK_GLOBAL_MEM_FENCE);
	}
}


__global float d_sumJDelta1;
__global float d_sumJDelta2;

__kernel void gradientDescentGPU_OPENCL_Optimized(__global float* J_hist, 
	__global float* x, __global float* y, 
	__global float* theta, int m, 
	float alpha, int iter, 
	__global float* theta_history, int blockIter, 
	__local float* sSumTemp1, __local float* sSumTemp2)
{
	int idx = get_global_id(0);
	int tidx = get_local_id(0);
	int bdimx = get_local_size(0);
	int gdimx = get_num_groups(0);

	kernel_enqueue_flags_t costF_flags = CLK_ENQUEUE_FLAGS_NO_WAIT;
	ndrange_t costF_ndrange = ndrange_1D(get_global_size(0), get_local_size(0));

	__local float thetaTemps[2];
	__local float deltaTemps[2];
	float hx = 0.0f;
	float jTheta = 0.0f;
	int t = 0;
	float x_data;
	float y_data;
	int block = gdimx*bdimx;
	int index = 0;

	if(idx < m){
		theta[0] = 0.0f;
		theta[1] = 0.0f;

		thetaTemps[tidx] = 0.0f;
		sSumTemp1[tidx] = 0.0f;
		sSumTemp2[tidx] = 0.0f;
		work_group_barrier(CLK_LOCAL_MEM_FENCE);
		for(int i = 0; i < iter; i++){
			if(idx == 0){
				theta_history[t] = theta[0];
				theta_history[t+1] = theta[1];
				t++;
			}
			d_sumJDelta1 = 0.0f;
			d_sumJDelta2 = 0.0f;
			for(int j = 0; j < blockIter; j++){
				x_data = x[((idx*2)+1)+(block*2*j)];
				y_data = y[(idx)+(block*j)];
				hx = theta[0] + (x_data * theta[1]);
				jTheta = hx - y_data;
				sSumTemp1[tidx] = jTheta;
				sSumTemp2[tidx] = jTheta*x_data;
				work_group_barrier(CLK_LOCAL_MEM_FENCE);
				for(int s = 1; s < bdimx; s*=2){
					index = s*2*tidx;
					if(index < bdimx){
						sSumTemp1[index] += sSumTemp1[index+s];
						sSumTemp2[index] += sSumTemp2[index+s];
					}
				}

				work_group_barrier(CLK_LOCAL_MEM_FENCE);
				if(tidx == 0){
					atomic_add_global(&d_sumJDelta1, sSumTemp1[0]);
					atomic_add_global(&d_sumJDelta2, sSumTemp2[0]);
				}
				work_group_barrier(CLK_LOCAL_MEM_FENCE);
			}

			deltaTemps[0] = d_sumJDelta1/m;
			deltaTemps[1] = d_sumJDelta2/m;
			work_group_barrier(CLK_LOCAL_MEM_FENCE);

			thetaTemps[0] = theta[0];
			thetaTemps[1] = theta[1];
			work_group_barrier(CLK_LOCAL_MEM_FENCE);
			theta[0] = thetaTemps[0] - (alpha*deltaTemps[0]);
			theta[1] = thetaTemps[1] - (alpha*deltaTemps[1]);
			if(idx == 0){
				enqueue_kernel(get_default_queue(),
								costF_flags,
								costF_ndrange,
								^{computeCostGPU_CUDA_SMem1b1t_static_test(&J_hist[i], x, y, theta, m, blockIter);});
			}
			
		}
	}
}