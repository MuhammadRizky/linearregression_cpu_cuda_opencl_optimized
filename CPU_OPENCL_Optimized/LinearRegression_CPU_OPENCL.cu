#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

//OPEMCL Includes
#include <CL/cl.h>

using namespace std;

int readDataFile(char* filen, int num, float* x, float* y);
int computeCost(float &out, float* x, float* y, float* theta, int m);
int gradientDescent(float* J_hist, float* x, float* xWOOne, float* y, float* theta, int m, float alpha, int iter, float* tHist);

//OPENCL Helper
cl_device_id create_device();
cl_int oclGetPlatformID(cl_platform_id *clSelectedPlatformID);
bool createProgram(cl_context context, cl_device_id device, string filename, cl_program* program);
bool checkSuccess(cl_int err);
const char* getErrorString(cl_int err);

#define checkOCLSuccessD(err) checkSuccess((err), __FILE__, __LINE__)
inline void checkSuccess(cl_int err, const char* file, int line){

	if(err != CL_SUCCESS){
		printf("OpenCL Error: %s -- %s : %d\n", getErrorString(err), file, line);
		exit(1);
	}
}


bool checkOCLSuccess(cl_int err){

	if(err != CL_SUCCESS){
		printf("OpenCL Error: %s\n", getErrorString(err));
		return false;
	}
	return true;
}

bool isPowerOfTwo(int n){
	if(n == 0)return 0;
	while(n != 1){
		if(n%2 != 0){
			return 0;}
		n /= 2;}
	return 1;
}	

int main(int argc, char** argv)
{
	if(argc < 6){
		printf("<Usage> <path/filename> <Number Training Set> <numThread> <Number Iteration for Gradient Descent> <Alpha - Learning Rate, i.e 0.01>\n");
		printf("i.e. costFunction_LinearRegression_CPU_CUDA ex1data16k.txt 1024 32 10 0.01\n");
		return -1;
	}
	clock_t cpu_start, cpu_end;
	clock_t ocl_start, ocl_end;
	//GET data from file 
	char* filename = (char*)malloc(100*sizeof(char));

	sprintf(filename,"%s",argv[1]);
	int numTrainingSet = atoi(argv[2]);
	int numThreadPerBlock = atoi(argv[3]);
	int iter = atoi(argv[4]);
	float alpha = atof(argv[5]); //0.01

	if(!isPowerOfTwo(numTrainingSet)){
		int inc = 1;
		while(inc < numTrainingSet){
			inc = inc << 1;
		}

		inc = inc >> 1;
		numTrainingSet = inc;
	}
	 if(numTrainingSet <= 2){
		return 0;
	}

	printf("numTrainingSet : %d\n", numTrainingSet);

	//%% ======================= Part 2: Get Data =======================
	size_t sizeXY = numTrainingSet*sizeof(float);
	size_t sizeTheta = 2*sizeof(float);
	float* x = (float*)malloc(sizeXY);
	float* y = (float*)malloc(sizeXY);

	int ret = readDataFile(filename, numTrainingSet, x, y);

	x = (float*)realloc(x, sizeXY);
	y = (float*)realloc(y, sizeXY);

	for(int i=0; i < 10; i++)
	{
		printf("X[%d] = %f, Y[%d] = %f\n", i, x[i], i, y[i]);
	}
	//%% =================== Part 3: Cost and Gradient descent ===================
	printf("\n\n============================== CPU REGION =============================================\n");
	float* dataX1 = (float*)malloc(2*sizeXY);
	float* theta= (float*)malloc(sizeTheta);
	for(int i=0; i < 2; i++){
		theta[i] = 0.0;}
	int j = 0;
	for(int i =0; i< 2*numTrainingSet; i++){
		if( i % 2 == 0){
			dataX1[i] = 1.0;
		}else{
			dataX1[i] = x[j];
			j++;
		}
	}
	cpu_start = clock();
	//COMPUTE COST FUNCTION --- J(theta)
	float jCostF = 0.0;
	theta[0] = -1.0;
	theta[1] = 2.0;
	ret = computeCost(jCostF, dataX1, y, theta, numTrainingSet);
	printf("Theta[%f,%f] Result : ComputeCost = %f\n", theta[0], theta[1], jCostF);
	cpu_end = clock();
	double cpu_JCost_Exec_time = (double)(cpu_end - cpu_start)/CLOCKS_PER_SEC;


	//COMPUTE GRADIENT DESCENT --- J(theta)
	size_t sizeHist = iter*sizeof(float);
	float* j_history = (float*)malloc(sizeHist);
	float* theta_history = (float*)malloc(sizeHist*2);
	for(int i=0; i < iter; i++){
		j_history[i] = 0.0;
		theta_history[i] = 0.0;
		theta_history[i+iter] = 0.0;
	}
	for(int i=0; i < 2; i++){
		theta[i] = 0.0;
	}

	size_t memUsage_CPU = sizeXY + sizeXY + (2*sizeXY) + sizeTheta + sizeHist + (sizeHist*2);

	cpu_start = clock();
	printf("Theta[%f,%f] Result : ComputeCost = %f\n", theta[0], theta[1], jCostF);
	ret = gradientDescent(j_history, dataX1, x, y, theta, numTrainingSet, alpha, iter, theta_history);
	printf("Result from Gradient Descent - Theta[%f,%f] Result : ComputeCost = %f\n", theta[0], theta[1], j_history[iter-1]);
	cpu_end = clock();

	for(int i=0, j = 0; i<5; i++, j+=2){
		printf("[i = %d]Theta[%f,%f] Result : gradientDescent -  = %f\n", i, theta_history[j], theta_history[j+1], j_history[i]);
	}
	double GD_cpu_exec_time = (double)(cpu_end - cpu_start)/CLOCKS_PER_SEC;
	//SET BACK TO ZEROS
	for(int i=0; i < iter; i++){
		j_history[i] = 0.0;
		theta_history[i] = 0.0;
		theta_history[i+iter] = 0.0;
	}
	for(int i=0; i < 2; i++){
		theta[i] = 0.0;
	}

	//============================== CUDA REGION ======================================================
	printf("\n\n============================== OPENCL REGION =============================================\n");

	//Initialize_OPENCL Structures
	cl_device_id device;
	cl_context context;
	cl_command_queue commandQueue;
	cl_program program;
	int err = 0;
	//Create Device
	device = create_device();
	context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
	checkOCLSuccessD(err);
	commandQueue = clCreateCommandQueue(context, device, 0, &err);
	checkOCLSuccess(err);
	if(!createProgram(context, device, "linreg.cl", &program)){
		cerr << "Failed to create a program OPENCL " << __FILE__ << " : " << __LINE__ << endl;
		return 1;
	}
	printf("===========COMPUTE COST FUNCTION - LINEAR REGRESSION OPENCL===========\n");
	theta[0] = -1.0;
	theta[1] = 2.0;
	float *hResult = (float*)malloc(sizeof(float));
	hResult[0] = 0.0;
	cl_mem d_x;
	cl_mem d_y;
	cl_mem d_dataX1;
	cl_mem d_theta;
	cl_mem d_result;
	d_x = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeXY, NULL, &err);
	checkOCLSuccessD(err);
	d_y = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeXY, NULL, &err);
	checkOCLSuccessD(err);
	d_dataX1 = clCreateBuffer(context, CL_MEM_READ_ONLY, 2*sizeXY, NULL, &err);
	checkOCLSuccessD(err);
	d_theta = clCreateBuffer(context, CL_MEM_READ_WRITE, 2*sizeof(float), NULL, &err);
	checkOCLSuccessD(err);
	d_result = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(float), NULL, &err);
	checkOCLSuccessD(err);
	checkOCLSuccessD(clEnqueueWriteBuffer(commandQueue, d_x, CL_TRUE, 0, sizeXY, x, 0, NULL, NULL));
	checkOCLSuccessD(clEnqueueWriteBuffer(commandQueue, d_y, CL_TRUE, 0, sizeXY, y, 0, NULL, NULL));
	checkOCLSuccessD(clEnqueueWriteBuffer(commandQueue, d_dataX1, CL_TRUE, 0, 2*sizeXY, dataX1, 0, NULL, NULL));
	checkOCLSuccessD(clEnqueueWriteBuffer(commandQueue, d_theta, CL_TRUE, 0, 2*sizeof(float), theta, 0, NULL, NULL));

	ocl_start = clock();
	int iter2 = 1;
	dim3 tpb2(numTrainingSet);
	dim3 bpg2(1);
	if(numTrainingSet > numThreadPerBlock){
		tpb2.x = numThreadPerBlock;
		iter2 = ceil(numTrainingSet/(numThreadPerBlock));
		bpg2.x = 1;
	}
	else if(numTrainingSet < numTrainingSet){
		return -1;
	}

	size_t workGlobal[1] = {tpb2.x*bpg2.x};
	size_t workLocal[1] = {tpb2.x};

	cl_kernel kernel;
	kernel = clCreateKernel(program, "computeCostGPU_CUDA_SMem1b1t_static", &err);
	checkOCLSuccessD(err);
	checkOCLSuccessD(clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_result));
	checkOCLSuccessD(clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_dataX1));
	checkOCLSuccessD(clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_y));
	checkOCLSuccessD(clSetKernelArg(kernel, 3, sizeof(cl_mem), &d_theta));
	checkOCLSuccessD(clSetKernelArg(kernel, 4, sizeof(int), &numTrainingSet));
	checkOCLSuccessD(clSetKernelArg(kernel, 5, sizeof(int), &iter2));
	
	printf("NumThreads = %d, numBlocks = %d, numTrainingSet = %d, Iteration = %d\n", tpb2.x, bpg2.x, numTrainingSet, iter2);
	checkOCLSuccessD(clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, workGlobal, workLocal, 0, NULL, NULL));
	checkOCLSuccessD(clFinish(commandQueue));
	checkOCLSuccessD(clReleaseKernel(kernel));
	checkOCLSuccessD(clEnqueueReadBuffer(commandQueue, d_result, CL_TRUE, 0, sizeof(float), hResult, 0, NULL, NULL));
	printf("Theta[%f, %f] - Output from ComputeCost GPU CUDA - NON ZERO COPY, Memcpy Async (compute sumReduce on Device) : Result = %f\n", theta[0], theta[1], hResult[0]);
	ocl_end = clock();
	double computeCostGPU_OCL_ExecTime = (double)(ocl_end - ocl_start)/CLOCKS_PER_SEC;
	theta[0] = 0.0;
	theta[1] = 0.0;

	printf("\n\n===========GRADIENT DESCENT OPENCL Optimized===========\n");
	ocl_start = clock();
	cl_mem d_jHist;
	cl_mem d_thetaHist;
	d_jHist = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeHist, NULL, &err);
	checkOCLSuccessD(err);
	d_thetaHist = clCreateBuffer(context, CL_MEM_WRITE_ONLY, 2*sizeHist, NULL, &err);
	checkOCLSuccessD(err);
	cl_kernel gradDescKernel = clCreateKernel(program, "gradientDescentGPU_OPENCL_Optimized", &err);
	checkOCLSuccessD(err);
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 0, sizeof(cl_mem), &d_jHist));
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 1, sizeof(cl_mem), &d_dataX1));
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 2, sizeof(cl_mem), &d_y));
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 3, sizeof(cl_mem), &d_theta));
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 4, sizeof(int), &numTrainingSet));
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 5, sizeof(float), &alpha));
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 6, sizeof(int), &iter));
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 7, sizeof(cl_mem), &d_thetaHist));
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 8, sizeof(int), &iter2));
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 9, tpb2.x*sizeof(float), NULL));
	checkOCLSuccessD(clSetKernelArg(gradDescKernel, 10, tpb2.x*sizeof(float), NULL));
	printf("NumThreads = %d, numBlocks = %d, numTrainingSet = %d, Iteration = %d\n", tpb2.x, bpg2.x, numTrainingSet, iter2);
	checkOCLSuccessD(clEnqueueNDRangeKernel(commandQueue, gradDescKernel, 1, NULL, workGlobal, workLocal, 0, NULL, NULL));
	checkOCLSuccessD(clFinish(commandQueue));
	checkOCLSuccessD(clReleaseKernel(gradDescKernel));
	checkOCLSuccessD(clEnqueueReadBuffer(commandQueue, d_jHist, CL_TRUE, 0, sizeHist, j_history, 0, NULL, NULL));
	checkOCLSuccessD(clEnqueueReadBuffer(commandQueue, d_thetaHist, CL_TRUE, 0, 2*sizeHist, theta_history, 0, NULL, NULL));
	checkOCLSuccessD(clEnqueueReadBuffer(commandQueue, d_theta, CL_TRUE, 0, 2*sizeof(float), theta, 0, NULL, NULL));
	ocl_end = clock();
	double GD_OPENCL_exec_time = (double)(ocl_end - ocl_start)/CLOCKS_PER_SEC;
	printf("jHist = %f,\nx = %f\nxWoone = %f\ny = %f\ntheta[%f, %f]\nm = %d\nalpha = %f,\niter = %d,\nthetaHist = %f\n\n", 
			j_history[0], dataX1[0], x[0], y[0], theta[0], theta[1], numTrainingSet, alpha, iter, theta_history[0]);
	printf("Result from Gradient Descent CUDA - Theta[%f,%f] Result : ComputeCost = %f\n", theta[0], theta[1], j_history[iter-1]);
	for(int i=0, j = 0; i<5; i++, j+=2){
		printf("[i = %d]Theta[%f,%f] Result : gradientDescent -  = %f\n", i, theta_history[j], theta_history[j+1], j_history[i]);
	}
	double memUsage_dcpu = (double) memUsage_CPU;
	printf("Memory Usage CPU - Total = %f MB\n", memUsage_dcpu/(1024*1024));
	printf("computeCost CPU HOST Execution Time : %lf\n", cpu_JCost_Exec_time);
	printf("computeCost OPENCL NON Zero copy Execution Time : %lf\n", computeCostGPU_OCL_ExecTime);
	printf("Gradient Descent CPU Execution Time : %lf\n", GD_cpu_exec_time);
	printf("Gradient Descent OPENCL Execution Time : %lf\n", GD_OPENCL_exec_time);
	//CLEAN UP
	checkOCLSuccessD(clReleaseMemObject(d_x));
	checkOCLSuccessD(clReleaseMemObject(d_dataX1));
	checkOCLSuccessD(clReleaseMemObject(d_y));
	checkOCLSuccessD(clReleaseMemObject(d_theta));
	checkOCLSuccessD(clReleaseMemObject(d_result));
	checkOCLSuccessD(clReleaseMemObject(d_jHist));
	checkOCLSuccessD(clReleaseMemObject(d_thetaHist));
	free(x);
	free(y);
	free(dataX1);
	free(theta);
	free(theta_history);
	free(j_history);

	return 0;
}

cl_device_id create_device(){

	cl_platform_id platform;
	cl_device_id device;
	int err;
	char platform_name[128];
	char device_name[128];

	err = oclGetPlatformID(&platform);

	size_t ret_param_size = 0;
	err = clGetPlatformInfo(platform, CL_PLATFORM_NAME, sizeof(platform_name), platform_name, &ret_param_size);
	if(err < 0){
		perror("Couldn't identify a Platform");
		exit(1);
	}else{
		printf("CL_PLATFORM_NAME : \t%s\n", platform_name);
	}

	//Access a device
	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
	err = clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(device_name), device_name, &ret_param_size);
	printf("Device found on the above platform: %s\n",device_name );
	if(err < 0){
		cerr << getErrorString(err) << __FILE__ << " : " << __LINE__ << endl;
		exit(1);
	}
	return device;
}

cl_int oclGetPlatformID(cl_platform_id *clSelectedPlatformID){
	char chBuffer[1024];
	cl_uint numPlatforms;
	cl_platform_id *clPlatformIDs;
	cl_int clErrNum;
	*clSelectedPlatformID = NULL;
	//Get OpenCL platform count
	clErrNum = clGetPlatformIDs(0, NULL, &numPlatforms);
	if(clErrNum != CL_SUCCESS){
		printf("Error %i in clGetPlatformIDs call !!!\n\n", clErrNum);
		return -1000;
	}
	else{
		if(numPlatforms == 0){
			printf("No OpenCL platform found!\n\n");
			return -2000;
		}
		else{
			//If there's a platform or more, make space for ID's
			if((clPlatformIDs = (cl_platform_id*)malloc(numPlatforms*sizeof(cl_platform_id))) == NULL){
				printf("Failed to allocate memory for cl_platform ID's\n\n");
				return -3000;
			}

			//Get platform info for each platform and trap NVIDIA platform if found
			clErrNum = clGetPlatformIDs(numPlatforms, clPlatformIDs, NULL);
			for(cl_uint i = 0; i < numPlatforms; ++i){
				clErrNum = clGetPlatformInfo(clPlatformIDs[i], CL_PLATFORM_NAME, 1024, &chBuffer, NULL);
				if(clErrNum == CL_SUCCESS){
					if(strstr(chBuffer, "NVIDIA") != NULL){
						*clSelectedPlatformID = clPlatformIDs[i];
						break;
					}
				}
			}
			//default to target platform if NVIDIA is not found
			if(*clSelectedPlatformID == NULL){
				printf("WARNING: NVIDIA OpenCL platform is not found - defaulting to first platform\n\n");
				*clSelectedPlatformID = clPlatformIDs[0];
			}
			free(clPlatformIDs);
		}
	}
	return CL_SUCCESS;
}

bool createProgram(cl_context context, cl_device_id device, string filename, cl_program* program){

	cl_int err = 0;
    ifstream kernelFile(filename.c_str(), ios::in);

    if(!kernelFile.is_open())
    {
        cerr << "Unable to open " << filename << ". " << __FILE__ << ":"<< __LINE__ << endl;
        return false;
    }
    ostringstream outputStringStream;
    outputStringStream << kernelFile.rdbuf();
    string srcStdStr = outputStringStream.str();
    const char* charSource = srcStdStr.c_str();

	*program = clCreateProgramWithSource(context, 1, &charSource, NULL, &err);
	checkOCLSuccess(err);
	if(program == NULL){
		cerr << "Failed to create OpenCL Program. " << __FILE__ << " : " << __LINE__ << endl;
		return false;
	}

	const char options[] = {"-cl-std=CL2.0"};

	// Try to build the OpenCL program
	bool buildSuccess = checkOCLSuccess(clBuildProgram(*program, 0, NULL, options, NULL, NULL));


	size_t logSize = 0;
	clGetProgramBuildInfo(*program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);

	if (logSize > 1){
		char* log1 = new char[logSize];
		clGetProgramBuildInfo(*program, device, CL_PROGRAM_BUILD_LOG, logSize, log1, NULL);

		string* stringChars = new string(log1, logSize);
		cerr << "Build Log : \n" << *stringChars << endl;

		delete[] log1;
		delete stringChars;
	}

	if(!buildSuccess){
		clReleaseProgram(*program);
		cerr << "Failed to build OpenCL program. " << __FILE__ << " : " << __LINE__ << endl;
		return false;
	}

	return true;
}

const char* getErrorString(cl_int error){

	switch(error){
    // run-time and JIT compiler errors
    case 0: return "CL_SUCCESS";
    case -1: return "CL_DEVICE_NOT_FOUND";
    case -2: return "CL_DEVICE_NOT_AVAILABLE";
    case -3: return "CL_COMPILER_NOT_AVAILABLE";
    case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
    case -5: return "CL_OUT_OF_RESOURCES";
    case -6: return "CL_OUT_OF_HOST_MEMORY";
    case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
    case -8: return "CL_MEM_COPY_OVERLAP";
    case -9: return "CL_IMAGE_FORMAT_MISMATCH";
    case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
    case -11: return "CL_BUILD_PROGRAM_FAILURE";
    case -12: return "CL_MAP_FAILURE";
    case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
    case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case -15: return "CL_COMPILE_PROGRAM_FAILURE";
    case -16: return "CL_LINKER_NOT_AVAILABLE";
    case -17: return "CL_LINK_PROGRAM_FAILURE";
    case -18: return "CL_DEVICE_PARTITION_FAILED";
    case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

    // compile-time errors
    case -30: return "CL_INVALID_VALUE";
    case -31: return "CL_INVALID_DEVICE_TYPE";
    case -32: return "CL_INVALID_PLATFORM";
    case -33: return "CL_INVALID_DEVICE";
    case -34: return "CL_INVALID_CONTEXT";
    case -35: return "CL_INVALID_QUEUE_PROPERTIES";
    case -36: return "CL_INVALID_COMMAND_QUEUE";
    case -37: return "CL_INVALID_HOST_PTR";
    case -38: return "CL_INVALID_MEM_OBJECT";
    case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case -40: return "CL_INVALID_IMAGE_SIZE";
    case -41: return "CL_INVALID_SAMPLER";
    case -42: return "CL_INVALID_BINARY";
    case -43: return "CL_INVALID_BUILD_OPTIONS";
    case -44: return "CL_INVALID_PROGRAM";
    case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
    case -46: return "CL_INVALID_KERNEL_NAME";
    case -47: return "CL_INVALID_KERNEL_DEFINITION";
    case -48: return "CL_INVALID_KERNEL";
    case -49: return "CL_INVALID_ARG_INDEX";
    case -50: return "CL_INVALID_ARG_VALUE";
    case -51: return "CL_INVALID_ARG_SIZE";
    case -52: return "CL_INVALID_KERNEL_ARGS";
    case -53: return "CL_INVALID_WORK_DIMENSION";
    case -54: return "CL_INVALID_WORK_GROUP_SIZE";
    case -55: return "CL_INVALID_WORK_ITEM_SIZE";
    case -56: return "CL_INVALID_GLOBAL_OFFSET";
    case -57: return "CL_INVALID_EVENT_WAIT_LIST";
    case -58: return "CL_INVALID_EVENT";
    case -59: return "CL_INVALID_OPERATION";
    case -60: return "CL_INVALID_GL_OBJECT";
    case -61: return "CL_INVALID_BUFFER_SIZE";
    case -62: return "CL_INVALID_MIP_LEVEL";
    case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
    case -64: return "CL_INVALID_PROPERTY";
    case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
    case -66: return "CL_INVALID_COMPILER_OPTIONS";
    case -67: return "CL_INVALID_LINKER_OPTIONS";
    case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

    // extension errors
    case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
    case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
    case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
    case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
    case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
    case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
    default: return "Unknown OpenCL error";
    }
}

int readDataFile(char* filen, int num, float* x, float* y)
{
	FILE* fdata;
	int numLines = 0;
	int i = 0;
	fdata = fopen(filen, "r");
	if(fdata == NULL){
		perror("Failed to read file. Check the name of input file");
		return 1;
	}
	double xtemp =0.00;
	double ytemp = 0.00;

	while(EOF != fscanf(fdata,"%lf,%lf\n",&xtemp,&ytemp)){
		if(i < num){
			x[i] = (float) xtemp;
			y[i] = (float) ytemp;
		}
		i++;
		numLines++;
	}
	printf("Number Lines : %d\n", numLines);
	fclose(fdata);
	return 0;
}

int computeCost(float &out, float* x, float* y, float* theta, int m){
	float hx = 0.0;
	float jTheta = 0.0;
	float sumJ = 0.0;
	for(int j = 0, k = 0; j < m; j++, k+=2){
		hx = (x[k]*theta[0]) + (x[k+1] * theta[1]);
		jTheta = hx - y[j];
		sumJ += powf(jTheta,2);
	}
	out = sumJ/(2*m);
	return 0;
}

int gradientDescent(float* J_hist, float* x, float* xWOOne, float* y, 
	float* theta, int m, float alpha, int iter, float* theta_history){
	float delta1 = 0.0;
	float delta2 = 0.0;
	float hx = 0.0;
	float jTheta = 0.0;
	float sumJDelta1 = 0.0;
	float sumJDelta2 = 0.0;
	int ret = 0;
	int t = 0;
	float tempT1 = 0.0;
	float tempT2 = 0.0;

	for(int i=0; i<iter ;i++){
		theta_history[t] = theta[0];
		theta_history[t+1] = theta[1];
		t+=2;
		sumJDelta1 = 0.0;
		sumJDelta2 = 0.0;
		for(int j = 0, k = 0; j < m; j++, k+=2)
		{
			hx = (x[k]*theta[0]) + (x[k+1] * theta[1]);
			jTheta = hx - y[j];
			sumJDelta1 += jTheta;
			sumJDelta2 += (jTheta*xWOOne[j]);
		}
		delta1 = sumJDelta1/m;
		delta2 = sumJDelta2/m;
		tempT1 = theta[0];
		tempT2 = theta[1];
		theta[0] = tempT1 - (alpha*delta1);
		theta[1] = tempT2 - (alpha*delta2);
		ret = computeCost(J_hist[i], x, y, theta, m);
	}
	return ret;
}